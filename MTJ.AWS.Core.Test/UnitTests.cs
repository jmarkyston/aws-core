using Amazon.S3;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MTJ.AWS.Core.Test
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void Construct()
        {
            var client = new AwsClientBase<IAmazonS3>();
            Assert.IsNotNull(client);
        }

        [TestMethod]
        public void ConstructHardcoded()
        {
            var client = new AwsClientBase<IAmazonS3>("KEY", "SECRET", "us-east-2");
            Assert.IsNotNull(client);
        }
    }
}
