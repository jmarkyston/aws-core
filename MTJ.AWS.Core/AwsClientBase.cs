﻿using Amazon;
using Amazon.Extensions.NETCore.Setup;
using Amazon.Runtime;
using Microsoft.Extensions.Configuration;

namespace MTJ.AWS.Core
{
    public class AwsClientBase<T> : IAwsClient<T> where T : IAmazonService
    {
        private string _Region;
        public string Region => _Region ?? Configuration.GetSection("AWS").GetSection("Region").Value;

        protected IConfiguration Configuration;

        public T ServiceClient { get; private set; } = default(T);

        private void Construct(IConfiguration configuration)
        {
            Configuration = configuration;
            AWSOptions options = configuration.GetAWSOptions();
            Construct(options);
        }
        private void Construct(AWSOptions options)
        {
            ServiceClient = options.CreateServiceClient<T>();
        }
        public AwsClientBase()
        {
            IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            Construct(configuration);
        }
        public AwsClientBase(IConfiguration configuration)
        {
            Construct(configuration);
        }
        public AwsClientBase(string accessKey, string secretKey, string region)
        {
            _Region = region;
            var options = new AWSOptions()
            {
                Credentials = new BasicAWSCredentials(accessKey, secretKey),
                Region = RegionEndpoint.GetBySystemName(region)
            };
            Construct(options);
        }
    }
}
