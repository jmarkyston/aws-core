﻿using Amazon.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Core
{
    public interface IAwsClient<T> where T : IAmazonService
    {
        T ServiceClient { get; }
    }
}
